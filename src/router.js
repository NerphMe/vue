import Vue from "vue";
import Router from "vue-router"
import GetApi from "./components/GetApi"
import Home from "./components/Home"
import Post from "./components/Post"

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '',
            component: Home
        },
        {
            path: '/get',
            component: GetApi
        },
        {
            path: '/post',
            component: Post
        },
    ]
})


